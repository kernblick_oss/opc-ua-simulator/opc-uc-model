# Create a NodeSet2 Model

This allows to create a basic model design that is shared among different specific models. The model used here is an incomplete example for a production machine.

## Installation Requirements

* Install Software
  * Python 3
    * pip install lxml
  * Podman (or docker)
  * Java

## Build Model

Start podman:

```bash
podman machine start
```

To build the OPC UA NodeSet2 XML, you need to create the corresponding Model Design XML files (see [1] and [2]). Then use the `compile.py` to compile the NodeSet2, e.g.:

```bash
python scripts/compile.py TestMachine
```

## Run the Model

For running the NodeSet2.xml in a test OPC UA server, the [opc-ua-server](https://gitlab.com/kernblick_oss/opc-ua-simulator/opc-ua-server) can be used:

```bash
set NODESET2=target\TestMachine\Opc.Ua.Kernblick.NodeSet2.xml
java -jar opcua-server-1.0.5.jar
```

Use an OPC UA client [3] to connect to the server.

## References
 - [1] https://profanter.medium.com/opc-ua-modeldesign-cheat-sheet-graphical-annotation-6eda944f0418
 - [2] https://profanter.medium.com/how-to-create-custom-opc-ua-information-models-1e9a461f5b58
 - [3] https://www.unified-automation.com/de/downloads/opc-ua-clients.html
 - [4] https://profanter.medium.com/
