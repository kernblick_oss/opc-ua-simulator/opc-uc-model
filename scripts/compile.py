import os
import subprocess
import shutil
import sys
import re

from createMergedModels import copy_xml_nodes

DOCKER_CMD = "podman"

def create_merged_model(input_xml, output_xml):
    copy_xml_nodes(input_xml, ["./*"], "src/BaseModel.xml", ".", output_xml)

def create_directories(directories):
    for directory in directories:
        if not os.path.exists(directory):
            os.makedirs(directory)

def copy_csv_files(csv_files):
    for csv_file in csv_files:
        if os.path.exists(f"src/{csv_file}.csv"):
            shutil.copy(f"src/{csv_file}.csv", f"target/merged/Merged{csv_file}.csv")

def run_docker_command(input_folder, output_folder):
    subprocess.call([DOCKER_CMD, "run", "-v", f"{os.getcwd()}:/data", "--rm", "--entrypoint", "/app/PublishModel.sh",
                     "sailavid/ua-modelcompiler", f"/data/"+input_folder, output_folder, "/data/"])

def is_numeric(v):
    try:
        f=int(v)
    except ValueError:
        return False
    return True

def replace_numeric_ids(file_path):
    lines = []
    with open(file_path) as infile:
        for line in infile:

            match = re.search(r'^([^,]*),\"?([^,]*)\"?,([^,]*)$', line)

            if is_numeric(match.group(2)):
                line = f"{match.group(1)},\"{match.group(1)}\",{match.group(3)}"

            lines.append(line)

    with open(file_path, 'w') as outfile:
        for line in lines:
            outfile.write(line)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python compile.py model1 model2 ...")
        sys.exit(1)

    models = sys.argv[1:]

    for model in models:
        input_xml = f"src/{model}.xml"
        output_xml = f"target/merged/Merged{model}.xml"
        directory = f"target/{model}"

        create_directories([directory, f"target/merged"])

        create_merged_model(input_xml, output_xml)


        csv_file = f"{model}"
        copy_csv_files([csv_file])

        input_folder = f"target/merged/Merged{model}"
        run_docker_command(input_folder, directory)

        # Replace numeric IDs and rerun docker command
        replace_numeric_ids(f"target/merged/Merged{csv_file}.csv")
        run_docker_command(input_folder, directory)

