import argparse
from lxml import etree as ET

def copy_xml_nodes(source_file, source_node_paths, target_file, target_node_path, output_file=None):
    # Parse the target XML file
    target_tree = ET.parse(target_file)
    target_root = target_tree.getroot()

    # Find the specific target node using the XPath
    target_node = target_root.find(target_node_path)
    if target_node is None:
        raise ValueError("Target node not found in the target XML file.")

    for source_node_path in source_node_paths:
        # Parse the source XML file
        source_tree = ET.parse(source_file)
        source_root = source_tree.getroot()

        # Find all matching source nodes using the XPath
        source_nodes = source_root.xpath(source_node_path)
        if not source_nodes:
            print("Source nodes not found in the source XML file for XPath:", source_node_path)
            continue

        # Append copies of the source nodes to the target node
        for source_node in source_nodes:
            target_node.append(source_node)

    # Write the updated XML tree to the output file or target file if output_file is not provided
    if output_file:
        with open(output_file, 'wb') as f:
            f.write(ET.tostring(target_tree, encoding='utf-8', pretty_print=True))
    else:
        with open(target_file, 'wb') as f:
            f.write(ET.tostring(target_tree, encoding='utf-8', pretty_print=True))

def main():
    parser = argparse.ArgumentParser(description='Copy specific XML nodes from one file to another.')
    parser.add_argument('source_file', type=str, help='Path to the source XML file')
    parser.add_argument('source_node_paths', nargs='+', type=str, help='XPaths of the source nodes in the source file')
    parser.add_argument('target_file', type=str, help='Path to the target XML file')
    parser.add_argument('target_node_path', type=str, help='XPath of the target node in the target file')
    parser.add_argument('-o', '--output_file', type=str, dest='output_file', help='Optional path to the output XML file')
    args = parser.parse_args()

    try:
        copy_xml_nodes(args.source_file, args.source_node_paths, args.target_file, args.target_node_path, args.output_file)
        print("XML nodes copied successfully.")
    except Exception as e:
        print("Error while copying XML nodes:", str(e))

if __name__ == "__main__":
    main()
